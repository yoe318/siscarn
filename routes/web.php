<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/', function () {
             return Redirect::to(route('home'));    
    });

    Route::resources([
       'tipos' => 'Configurar\TiposController',
       'cargos' => 'Configurar\CargosController',
       'ubicaciones' => 'Configurar\UbicacionController',
       'personal' => 'Carnets\PersonalController',
       'carnet' => 'Carnets\CarnetController',
       'usuarios' => 'Configurar\UsuariosController',  
      ]);
       
    
    Route::get('buscar','AjaxController@buscar')->name('buscar');
    Route::get('buscar_trabajador', 'AjaxController@buscar_trabajador')->name('buscar_trabajador');
     Route::get('buscar_ciudadano', 'AjaxController@buscar_ciudadano')->name('buscar_ciudadano');
    Route::post('estatus/actualizar','Configurar\EstatusController@actualizar')->name('estatus.actualizar');


    Route::post('estatus/eliminar','Configurar\EstatusController@eliminar')->name('estatus.eliminar');

    Route::post('procedencia/actualizar','Configurar\ProcedenciaController@actualizar')->name('procedencia.actualizar');

    Route::post('procedencia/eliminar','Configurar\ProcedenciaController@eliminar')->name('procedencia.eliminar');

    Route::get('imprimir/{id}','Carnets\CarnetController@imprimir')->name('carnet.imprimir');

    Route::post('tipo/actualizar','Configurar\TipoController@actualizar')->name('tipo.actualizar');

    Route::post('tipo/eliminar','Configurar\TipoController@eliminar')->name('tipo.eliminar');
    Route::get('buscarModelo','AjaxController@buscarModelo')->name('buscarModelo');
    Route::post('solicitud/buscar','Registro\SolicitudController@buscar')->name('solicitud.buscar');    
        
    Route::post('solicitud/buscarcategoria','Registro\SolicitudController@buscar_categorias')->name('solicitud.buscarcategoria');    
        
    Route::get('solicitud/destroy/{id}', [
            'uses' => 'Registro\SolicitudController@destroy',
            'as'   => 'solicitud.destroy'
           ]);

   

    Route::post('categorias/actualizar','Configurar\CategoriasController@actualizar')->name('categorias.actualizar');

    Route::post('categorias/eliminar','Configurar\CategoriasController@eliminar')->name('categorias.eliminar');  

    Route::post('usuarios/estatus','Configurar\UsuariosController@status')->name('usuarios.estatus');
           Route::get('/usuarios/cambiar/{id}', [
            'uses' => 'Configurar\UsuariosController@cambiar',
            'as'   => 'usuarios.cambiar'
           ]);

    Route::put('/usuarios/update_password/{valor}', [
                'uses' => 'Configurar\UsuariosController@update_password',
                'as'   => 'usuarios.update_password'
            ]);

    Route::post('crear/usuario','UsuariosController@store')->name('crear.usuario');
           
   

    
});    
