@extends('layouts.layout')
@section('link_back_none','d-none')
@section('link_new_none','d-none')
@section('content')
            <div class="container-xxl flex-grow-1 container-p-y">
              <div class="row">
                <div class="col-lg-8 mb-4 order-0">
                  <div class="card">
                    <div class="d-flex align-items-end row">
                      <div class="col-sm-7">
                        <div class="card-body">
                          <h5 class="card-title text-primary">Últimos Registros 🎉</h5>
                         <!-- <p class="mb-4">
                            You have done <span class="fw-bold">72%</span> more sales today. Check your new badge in
                            your profile.
                          </p>-->

                         <!-- <a href="javascript:;" class="btn btn-sm btn-outline-primary">View Badges</a>-->
      <div class="table-responsive text-nowrap">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Nombres</th>
                        <th>Tipo</th>
                        <th>Fecha</th>
                      </tr>
                    </thead>
                    <tbody class="table-border-bottom-0">
                      @foreach($carnet as $carnet)
                      <tr>
                        <td><i class="fab fa-angular fa-lg text-danger me-3"></i> <strong>{{ $carnet->nombres  }} {{ $carnet->apellidos  }}</strong></td>
                        <td>{{ $carnet->tipo  }}</td>
                        <td><span class="badge bg-label-primary me-1">Active</span></td>
 
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>                   
                        </div>
                      </div>
                      <div class="col-sm-5 text-center text-sm-left">
                        <div class="card-body pb-0 px-0 px-md-4">
                          <img
                            src="{{ asset('sneat/assets/img/illustrations/man-with-laptop-light.png')}}"
                            height="140"
                            alt="View Badge User"
                            data-app-dark-img="{{ asset('sneat/illustrations/man-with-laptop-dark.png')}}"
                            data-app-light-img="{{ asset('sneat/illustrations/man-with-laptop-light.png')}}"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
  
                <!-- Total Revenue -->
             
                <!--/ Total Revenue -->
                <div class="col-12 col-md-8 col-lg-4 order-3 order-md-2">
                  <div class="row">
                    <div class="col-6 mb-4">
                     
                    
                    <!-- </div>
    <div class="row"> -->
                  </div>
                </div>
              </div>
              <div class="row">
                <!-- Order Statistics -->
 
                <!--/ Order Statistics -->

                <!-- Expense Overview -->
              
                <!--/ Expense Overview -->

                <!-- Transactions -->
          
                <!--/ Transactions -->
              </div>

@endsection
