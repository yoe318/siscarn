@extends('layouts.layout')
@section('titulo', 'Configurar')
@section('titulo2', 'Cargos')
 @section('link_back',route("cargos.index"))
@section('link_new_none','d-none')
@section('content')

            <div class="container-xxl flex-grow-1 container-p-y">
                <form action="{{ route('cargos.update',$cargo->id)}}" method="POST" enctype="multipart/form-data" >
                  {{ csrf_field() }}
@method('PUT') 
<input type="hidden" name="id" value="{{ $cargo->id }}">
              <div class="row">
              <div class="col-md-6">
                  <div class="card mb-4">
                    <h5 class="card-header">Editar</h5>
                    <div class="card-body">
                      <div>
                        <label for="defaultFormControlInput" class="form-label">Descripción y/o Nombre</label>
                        <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Descripcion" aria-describedby="defaultFormControlHelp" value="{{ $cargo->descripcion }}">
                        <div id="defaultFormControlHelp" class="form-text">
                          Ingrese cargo de Carnet
                        </div>
                      </div>
                    </div>
                      <div class="card mb-2">
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                  </div>
                </div>  


                </div>
                </form>
                <!--/ Transactions -->
              </div>

@endsection
