@extends('layouts.layout')
@section('titulo', 'Configurar')
@section('titulo2', 'Usuarios')
 @section('link_back',route("home"))
 @section('link_new',route('usuarios.create'))
@section('content')
  
 @if(Session::has('mensaje'))

<div class="alert alert-info alert-dismissible fade show" role="alert">
    <span class="alert-icon"><i class="ni ni-like-2"></i></span>
    <span class="alert-text"><strong>Éxito</strong> {!!Session::get('mensaje')!!}</span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
@if(Session::has('mensajeError'))

<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <span class="alert-icon"><i class="ni ni-like-2"></i></span>
    <span class="alert-text"><strong>Error!!</strong>  {!!Session::get('mensajeError')!!}</span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<div class="row">
 <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">     
            <div class="table-responsive">
          <table class="table table-striped data-table">
            <thead>
              <tr>
                <th>Usuario</th>
                <th>Correo Electrónico</th>
                <th>Rol</th>
                <th>Creado</th>
                <th>Status</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              @foreach($usuarios as $usuario)
              <tr>
                <td>{{ $usuario->name }}</td>
                <td>{{ $usuario->email }}</td>
                <td>{{ $usuario->descripcion }}</td>
                <td>{{ $usuario->created_at}}</td>
                <td>
                             @if($usuario->id_estado==1)
                             <div class="col">
                                <p class="mb-2">Activo</p>
                                <label class="toggle-switch toggle-switch-success">
                                 <input type="checkbox" id="status" name="status" data-id="{{$usuario->id}}" checked="">
                                  <span class="toggle-slider round"></span>
                                  </label>                      
                             </div>
                             @else
                             <div class="col">
                        <p class="mb-2">Deshabilitado</p>
                      <label class="toggle-switch toggle-switch-dark">
                        <input type="checkbox" id="status" name="status" data-id="{{$usuario->id}}">
                        <span class="toggle-slider round"></span>
                      </label>                      
                    </div>
                   @endif
                  
                </td>
                <td class="td-actions">
                  <a class="btn btn-success btn-sm" href="{{ route('usuarios.edit',$usuario->id) }}" title="Editar Registro">
                              <i class="typcn typcn-edit btn-icon-append">Editar</i>                          
                            </a>

                    <?php 
                    $valor = $usuario->id; 
                    $url="usuarios/cambiar/".$valor; ?>
                    <a data-toggle="tooltip" data-placement="top" class="btn btn-info btn-sm" href="{{ url($url) }}" title="Cambiar Contraseña">Cambiar<i class="mdi mdi-account-key"></i></a>

              {{--     @switch($usuario->id_estado)
                      @case(1)


                      <a style="font-size: 1.2rem" type="button" rel="tooltip" title="Remover" href="{{ route('usuarios.estatus',$usuario->id)}}" class="btn btn-danger btn-sm">
                        <i class="mdi mdi-account-off "></i>
                    </a>
                          @break
                      @case(0)
                      <a style="font-size: 1.2rem" type="button" rel="tooltip" title="Activar" href="{{ route('usuarios.estatus',$usuario->id)}}" class="btn btn-warning btn-sm">
                        <i class=" mdi mdi-account-check "></i>
                    </a>
                          @break
                      @case(2)
                       <a style="font-size: 1.2rem" type="button" rel="tooltip" title="Activar" href="{{ route('usuarios.estatus',$usuario->id)}}" class="btn btn-warning btn-sm">
                        <i class=" mdi mdi-account-check "></i>
                    </a>
                    </button>
                          @break

                      @default                          
                  @endswitch  --}}              
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
 </div> 
@endsection
@push('scripts')
<script>
  
$(document).ready(function() {
  $('#status').on('click', function(e){
    id = $(this).data('id');
    console.log(id);
  estaSeleccionado = $('input:checkbox[name=status]:checked').val();
    if(estaSeleccionado == 'on') activo = 1;
    else activo = 0;
  

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  url="usuarios/estatus";
  $.ajax({
    type: "post",
    url: url,
    dataType: "json",
    cache: false,
    data: {id: id},
    success: function (data){
      console.log(data);
      location.reload();          
    }
  });


   });  
   
} );
  $(function () {

    

    var table = $('.data-table').DataTable({

      "language": {
      "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
    },      

    });

    

  });  

</script>

@endpush