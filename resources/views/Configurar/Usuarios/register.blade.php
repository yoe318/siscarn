@extends('layouts.layout')
@section('titulo', 'Configurar')
@section('titulo2', 'Usuarios')
 @section('link_back',route("usuarios.index"))
@section('link_new_none','d-none')
@section('content')

@section('content')
<div class="col-md-12">
<div class="card">
    <div class="card-header">
        <strong>{{ __('Nuevo Usuario') }}</strong> - Complete todos los campos
    </div>
    @if ( count( $errors ) > 0 )
                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                    <span class="badge badge-pill badge-danger">AVISO:</span> 
                        @foreach ($errors->all() as $error)
                            {{ $error }} <br>
                        @endforeach
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            @endif
    <div class="card-body card-block">
        <form action="{{ route('usuarios.store') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    @csrf
        <div class="row form-group">
                <div class="col col-md-3"><label for="email-input" class=" form-control-label">{{ __('Nombre') }}</label></div>
                <div class="col-12 col-md-9"><input placeholder="Nombre del Usuario" id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                    <small class="help-block form-text">Por favor ingresa el nombre del usuario</small></div>
        </div>
        <div class="row form-group">
                <div class="col col-md-3"><label for="email-input" class=" form-control-label">{{ __('Correo Electrónico') }}</label></div>
                <div class="col-12 col-md-9">
                    <input placeholder="Correo Electrónico" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email">
                </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="select" class=" form-control-label">{{ __('Rol de Usuario') }}</label></div>
            <div class="col-12 col-md-9">
            <select id="role_id" class="form-control" name="role_id" required>
                <option value="">Seleccione</option>
                @foreach($roles as $role)
                <option value="{{$role->id}}">{{$role->descripcion}}</option>
                @endforeach                        
            </select>                
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="password-input" class=" form-control-label">{{ __('Clave') }}</label></div>
            <div class="col-12 col-md-9">
                <input placeholder="Clave"  id="password" type="password" class="form-control" name="password" required autocomplete="new-password">                        
            </div>            
        </div>
        <div class="row form-group">
            <div class="col col-md-3"><label for="password-input" class=" form-control-label">{{ __('Confirmación Clave') }}</label></div>
            <div class="col-12 col-md-9">
                    <input placeholder="Confirmación Clave" id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">               
                <small class="help-block form-text">Por favor repita la contraseña</small></div>
        </div>
    </div>
        <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                <i class="fa fa-dot-circle-o"></i> {{ __('Registrar') }}
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                <i class="fa fa-ban"></i> Borrar
                </button>
            </div>
        </form>
</div>
</div>
@endsection
