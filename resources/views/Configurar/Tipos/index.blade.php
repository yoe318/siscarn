@extends('layouts.layout')
@section('titulo', 'Configurar')
@section('titulo2', 'Tipo Personal')
 @section('link_back',route("home"))
 @section('link_new',route('tipos.create'))
@section('content')
  <div class="container-xxl flex-grow-1 container-p-y">
@if(Session::has('mensaje'))

<div class="bs-toast toast fade show bg-primary" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
                          <i class="bx bx-bell me-2"></i>
              <div class="me-auto fw-semibold">Siscarn</div>
                <small>Exito</small>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
              <div class="toast-body">
                          {!!Session::get('mensaje')!!}
                  </div>
            </div>

@endif
@if(Session::has('mensajeError'))
<div class="bs-toast toast fade show bg-danger" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
                          <i class="bx bx-bell me-2"></i>
              <div class="me-auto fw-semibold">Siscarn</div>
                <small>Exito</small>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
              <div class="toast-body">
                          {!!Session::get('mensajeError')!!}
                  </div>
            </div>
@endif
<div class="row">
<div class="card">
                <h5 class="card-header">Lista</h5>
                <div class="table-responsive text-nowrap">
                  <table class="table data-table">
                    <thead class="table-light">
                      <tr>
                        <th>#</th>
                        <th>Descripción</th>
                        <th>Status</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody class="table-border-bottom-0">
                      @foreach($tipos as $key)
                      <tr>
                        <td>{{ $key->id }}</td>
                        <td>{{ $key->descripcion }}</td>
                       
                        <td><span class="badge bg-label-primary me-1">Active</span></td>
                        <td>
                          <div class="dropdown">
                            <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                              <i class="bx bx-dots-vertical-rounded"></i>
                            </button>
                            <div class="dropdown-menu">
                              <a class="dropdown-item" href="{{ route('tipos.edit',$key->id) }}"><i class="bx bx-edit-alt me-1"></i> Editar</a>
                              <a class="dropdown-item" href="{{ route('tipos.destroy',$key->id) }}"><i class="bx bx-trash me-1"></i> Delete</a>
                            </div>
                          </div>
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>                </div>
                <!--/ Transactions -->
              </div>

@endsection
@push("scripts")
<script type="text/javascript">
  $(function () {

    

    var table = $('.data-table').DataTable({

      "language": {
      "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
    },      

    });

    

  });  


</script>
@endpush