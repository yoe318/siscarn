@extends('layouts.layout')
@section('titulo', 'Configurar')
@section('titulo2', 'Tipo Personal')
 @section('link_back',route("tipos.index"))
@section('link_new_none','d-none')
@section('content')

            <div class="container-xxl flex-grow-1 container-p-y">
                <form action="{{ route('tipos.update',$tipo->id)}}" method="POST" enctype="multipart/form-data" >
                  {{ csrf_field() }}
@method('PUT') 
<input type="hidden" name="id" value="{{ $tipo->id }}">
              <div class="row">
              <div class="col-md-6">
                  <div class="card mb-4">
                    <h5 class="card-header">Editar</h5>
                    <div class="card-body">
                      <div>
                        <label for="defaultFormControlInput" class="form-label">Descripción y/o Nombre</label>
                        <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Descripcion" aria-describedby="defaultFormControlHelp" value="{{ $tipo->descripcion }}">
                        <div id="defaultFormControlHelp" class="form-text">
                          Ingrese Tipo de Carnet
                        </div>
                      </div>
                    </div>
                  </div>
                </div>  
<div class="col-md-6">
  <?php

 $archivo = 'img/tipo/'.$tipo->url_imagen;
 if (Storage::disk('public')->exists($archivo) == true)
    {
      $archivo = Storage::disk('public')->url($archivo);
    }
  else{
    $archivo="sneat/assets/img/elements/5.jpg";
   }
   
?>
  <div class="card mb-4">
      <h5 class="card-header">Plantilla de Diseño</h5>
      <div class="card-body">
          <div class="form-floating">
                        <input type="file" class="form-control" id="url_imagen" name="url_imagen">
                        <label for="floatingInput">Seleccione Imagen</label>
                        <div id="floatingInputHelp" class="form-text">
                          <img class="card-img-top" src="{{ asset($archivo) }}" id="imgSalida" alt="Card image cap">
                          
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
<div class="col-md-6">
  <div class="card mb-2">
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
</div>
                </div>
                </form>
                <!--/ Transactions -->
              </div>

@endsection
@push('scripts')

<script  type="text/javascript" charset="utf-8" >
   
  $('#url_imagen').change(function(e) {
      addImage(e); 
    });

   

    function addImage(e){
      var file = e.target.files[0],
      imageType = /image.*/;
      if (!file.type.match(imageType))
        return;

      var reader = new FileReader();
      reader.onload = fileOnload;
      reader.readAsDataURL(file);
    }

    function fileOnload(e) {
      var result=e.target.result;
      $('#imgSalida').attr("src",result);
    }

   

  
   
   
  



  
</script>



@endpush

