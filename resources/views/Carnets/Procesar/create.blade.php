@extends('layouts.layout')
@section('titulo', 'Carnets')
@section('titulo2', 'Crear')
 @section('link_back',route("carnet.index"))
@section('link_new_none','d-none')
@section('content')
@section('content')
            <div class="container-xxl flex-grow-1 container-p-y">
                <form action="{{ route('carnet.store')}}" method="POST" enctype="multipart/form-data" >
                  {{ csrf_field() }}
              <div class="row">
              <div class="col-md-6">
                  <div class="card mb-4">
                    <h5 class="card-header">Crear</h5>
                    <div class="card-body">
                      <div>
                        <label for="defaultFormControlInput" class="form-label">Cédula</label>
                        <input type="text" class="form-control" id="cedula" name="cedula" placeholder="Cédula" aria-describedby="defaultFormControlHelp">
                       
                      </div>
                    </div>

                   <div class="card-body">
                     <div class="mb-3">
                          <label class="form-label" for="basic-default-fullname">Apellidos</label>
                          <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="John Doe">
                        </div>
                        <div class="mb-3">
                          <label class="form-label" for="basic-default-fullname">Nombres</label>
                          <input type="text" class="form-control" id="nombres" name="nombres" placeholder="John Doe">
                        </div>    
                       
                       <div class="mb-3">
                        <label for="defaultSelect" class="form-label">Cargo</label>
                        <select id="id_cargo" name="id_cargo" class="form-select">
                          <option value="0">Seleccione</option>
                          
                        </select>
                      </div> 
                      <div class="mb-3">
                        <label for="defaultSelect" class="form-label">Ubicación</label>
                        <select id="id_ubicacion" name="id_ubicacion" class="form-select">
                          <option value="0">Seleccione</option>
                          
                        </select>
                      </div> 
                      <div class="mb-3">
                          <label class="form-label" for="basic-default-fullname">Fecha de Ingreso</label>
                          <input type="date" class="form-control" id="fecha_ingreso" name="fecha_ingreso" placeholder="John Doe">
                        </div> 


                    </div>

                  </div>
                </div>  
<div class="col-md-6">
                  <div class="card mb-4">
                    <h5 class="card-header">Foto</h5>
                    <div class="card-body">
                      <div class="form-floating">
                        <input type="file" class="form-control" id="url_imagen" name="url_imagen">
                        <label for="floatingInput">Seleccione Imagen</label>
                        <div id="floatingInputHelp" class="col-md-4">
                          <img class="card-img card-img-right" src="{{ asset('sneat/assets/img/elements/5.jpg') }}" id="imgSalida" alt="Card image cap">
                         
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
<div class="col-md-6">
  <div class="card mb-2">
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
</div>
                </div>
                </form>
                <!--/ Transactions -->
              </div>

@endsection
@push('scripts')

<script  type="text/javascript" charset="utf-8" >
   $("input#cedula").keydown(function (event) {

      if(event.shiftKey)
      {
        event.preventDefault();
      }
      if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 241 )    {
      }
      else {
        if (event.keyCode < 95) {
          if (event.keyCode < 48 || event.keyCode > 57) {
            event.preventDefault();
          }
        } 
        else {
          if (event.keyCode < 96 || event.keyCode > 105) {
            event.preventDefault();
          }
        }
      }        
      ;
    }); 

  $('#cedula').change(function(event){
  id = $(this).val();
   console.log(id);   
   $.ajaxSetup({
     headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
  
  $.ajax({
    type: "get",
    url: '{{route('buscar')}}',
    dataType: "json",
    cache: false,
    data: { valor: id },
    success: function (data){
      console.log(data);
      $('#apellidos').val(data.apellidos);
      $('#nombres').val(data.nombres);
      $('#apellidos').val(data.apellidos);
      $('#cargo').val(data.cargo);
    }
  });

});




  $('#url_imagen').change(function(e) {
      addImage(e); 
    });

   

    function addImage(e){
      var file = e.target.files[0],
      imageType = /image.*/;
      if (!file.type.match(imageType))
        return;

      var reader = new FileReader();
      reader.onload = fileOnload;
      reader.readAsDataURL(file);
    }

    function fileOnload(e) {
      var result=e.target.result;
      $('#imgSalida').attr("src",result);
    }

   

  
   
   
  



  
</script>



@endpush

