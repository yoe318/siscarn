<!DOCTYPE html>
<html>
<head>
  <title>Carnet</title>
  <link href="{{ asset('css/bootstrap.css') }}" rel='stylesheet' type='text/css' />

    <style type="text/css" media="screen">
        tr{
            font-size: .8rem;
          
        }
        p{
            font-size: .8rem;
          
        }
        span{
          font-size: .7rem;  
        }
        .div-1 {
          border: 5px outset #281f77;
          background-color: lightblue;    
          text-align: center;
          font-size: .6rem; 
          color: black; iu
     }
         .qr{
          position:absolute;
          height:80px;
          width:80px;
          top: 140px;
          left : 340px;
          z-index: 1;
        }

        .fondo
        {
          position:absolute;
          height:640px;
          width:480px;
          z-index: -1;
        }
.circuloseguridad {
        height: 45px;
        width:25px;
         border-radius: 50%;
        margin: 2px;
        border: 1px solid;
        background-color: yellow;
    }

  .acceso1 {
        height: 15px;
        width:25px;
        margin: 2px;
        border: 2px solid;
        background-color: yellow;
    }
    
    .acceso2 {
          height: 15px;
        width:25px;  
        margin: 2px;
        border: 2px solid;
        background-color: blue;
    }
    
    .acceso3 {
          height: 15px;
        width:25px;  
        margin: 2px;
        border: 2px solid;
        background-color: red;
    }
    </style>
</head>
<body>

   
                @php
                $url="";
                    $archivo2 = 'img/personal/'.$carnet->url_imagen;
              if (Storage::disk('public')->exists($archivo2) == true)
              {
                $archivo = Storage::disk('public')->url($archivo2);
              }
              else{
                $archivo = "sneat/assets/img/elements/5.jpg";
              }

               $archivo2 = 'img/tipo/'.$carnet->imagentipo;
              if (Storage::disk('public')->exists($archivo2) == true)
              {
                $archivot = Storage::disk('public')->url($archivo2);
              }
              else{
                $archivot = "sneat/assets/img/elements/5.jpg";
              }
              #dd($qrname);
                @endphp
   
  

   <div style="position: relative; left: 0; top: 0;" >
    
      
       <img src="{{ asset($archivot)}}" alt="" class="fondo"> 
  </div>

   <!--<div style="float: right; position: relative; top:95; right:-10; z-index: 1;">-->
    <div style="padding: 10px; margin: 10px; float: right; ">
      
  
  </div>
 
<div style="position:absolute; top:120; left:10; line-height : 25px; align='center'; ">
  <p style="font-family: Arial; font-size:12;"><b>{{$carnet->id}}</b> </p>
</div>  
<div style="position:absolute; top:130; left:260; line-height : 25px; align='center'; ">
        <img src="{{ $archivo}}" alt="" width="85%">
       
        
</div>  

<div style="position:absolute; top:194; left:260; line-height : 25px; align='center'; ">
        <img src="{{ $archivo}}" alt="" width="25%">
         
         
</div>  
<div style="position:absolute; top:196; left:60; align='center'; ">
      @if($carnet->tipo_acceso==1)
      <div class="acceso1">
         
       </div> 
      @endif   
     @if($carnet->tipo_acceso==2)
      <div class="acceso2">
         
       </div> 
      @endif 
       @if($carnet->tipo_acceso==3)
      <div class="acceso3">
         
       </div> 
      @endif                
       @if($carnet->tipo_acceso==4)
      <div class="acceso1">
         
       </div> 
       <div style="position:absolute; top:0; left:30; align='center'; ">
         <div class="acceso2"> 
          </div> 
       </div>
       <div style="position:absolute; top:0; left:60; align='center'; ">
         <div class="acceso3"> 
          </div> 
       </div>
      @endif                

</div> 

<div style="position:absolute; top:120; left:20; line-height : 25px; align='center'; ">
        
         <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(100)->generate($qrname)) !!} " class="qr">
        
</div>  




    <div style="position:absolute; top:220; left:55; line-height : 25px; align='center'; ">
       <p style="font-family: Arial; font-size:14;"><b>{{$carnet->codigo_seguridad}}</b>  <br>

        <b>{{$carnet->apellidos}}</b> <br>
        <b>{{$carnet->nombres}}</b> <br>
        <b>C.I.: {{$carnet->cedula}}</b><br>
        
        
    </p>
    
   </div>
 
 <div style="position:absolute; top:240; left:15; line-height : 25px; align='center'; ">
       
    <div class="circuloseguridad">
         
       </div> 
   </div>

 <div style="position:absolute; top:390; left:125; line-height : 25px; align='center'; ">
       <p style="font-family: Arial; font-size:14;text-align='center';text-transform: uppercase;">       
        <b>{{$carnet->ubicacion}}</b> <br>
        
    </p>
    
   </div>

   <div style="position:absolute; top:440; left:105; line-height : 25px; align='center'; ">
       <p style="font-family: Arial; font-size:14;text-align='center';text-transform: uppercase;">       
        <b>{{$carnet->cargo}}</b> <br>
        
    </p>
    
   </div>
   <br>
{{--   <div style="position:absolute; top:495; left:18; line-height : 25px; align:'center';">
       <p style="font-family: Arial; font-size:14; color: #FFF;"><small>Solo para uso dentro de las instalaciones de la Asamblea Nacional</small> 
    </p>
   </div> --}}    
   
   

</center>
<script src="{{ asset('js/bootstrap.js') }}"></script>
</body>
</html>