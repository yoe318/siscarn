@extends('layouts.layout')
@section('titulo', 'Carnetizacion')
@section('titulo2', 'carnet - Editar')
 @section('link_back',route("carnet.index"))
@section('link_new_none','d-none')
@section('content')
@section('content')
            <div class="container-xxl flex-grow-1 container-p-y">
                <form action="{{ route('carnet.update',$carnet->id)}}" method="POST" enctype="multipart/form-data" >
                  {{ csrf_field() }}
@method('PUT') 
<input type="hidden" name="id" value="{{ $carnet->id }}">
              <div class="row">
              <div class="col-md-6">
                  <div class="card mb-4">
                    <h5 class="card-header">Crear</h5>
                    <div class="card-body">
                      <div>
                        <label for="defaultFormControlInput" class="form-label">Cédula</label>
                        <input type="text" class="form-control" id="cedula" name="cedula" placeholder="Cédula" value="{{ $carnet->cedula }}" aria-describedby="defaultFormControlHelp">
                       
                      </div>
                    </div>

                   <div class="card-body">
                     <div class="mb-3">
                          <label class="form-label" for="basic-default-fullname">Apellidos</label>
                          <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="John Doe" value="{{ $carnet->apellidos }}">
                        </div>
                        <div class="mb-3">
                          <label class="form-label" for="basic-default-fullname">Nombres</label>
                          <input type="text" class="form-control" id="nombres" name="nombres" placeholder="John Doe" value="{{ $carnet->nombres }}">
                        </div>    
                       
<div class="mb-3">
                        <label for="defaultSelect" class="form-label">Cargo</label>
                        <input type="text" class="form-control" id="cargo" name="cargo" value="{{ $carnet->cargo }}">
                      </div> 
                      <div class="mb-3">
                        <label for="defaultSelect" class="form-label">Ubicación</label>
                          <input type="text" class="form-control" id="ubicacion" name="ubicacion" value="{{ $carnet->ubicacion }}">
                      </div> 
                      <div class="mb-3">
                          <label class="form-label" for="basic-default-fullname">Fecha de Ingreso</label>
                          <input type="text" class="form-control" id="fecha_ingreso" name="fecha_ingreso" value="{{ $carnet->fecha_ingreso }}" readonly="">
                        </div> 
                        <div class="mb-3">
                        <label for="defaultSelect" class="form-label">Tipo Personal</label>
                        <select id="id_tipo" name="id_tipo" class="form-select">
                          <option value="0">Seleccione</option>
                          @foreach($tipo as $key)
                           <option value="{{ $key->id }}" @if($key->id==$carnet->id_tipo) selected @endif>{{ $key->descripcion }}</option>
                        
                          @endforeach
                        </select>
                      </div> 
                      <div class="mb-3">
                          <label class="form-label" for="basic-default-fullname">Código de Seguridad</label>
                          <input type="number" class="form-control" id="codigo_seguridad" name="codigo_seguridad" placeholder="Código de Seguridad" value="{{$carnet->codigo_seguridad}}">
                      </div>
                    <div class="mb-3">
                          <label class="form-label" for="basic-default-fullname">Seleccione Tipo de Acceso</label>
                          <select id="tipo_acceso" name="tipo_acceso" class="form-select" value="{{$carnet->tipo_acceso}}">
                          <option value="0">Seleccione</option>
                             <option value="1" @if($carnet->tipo_acceso==1) selected="" @endif>Amarillo</option>
                             <option value="2" @if($carnet->tipo_acceso==2) selected="" @endif>Azul</option>
                             <option value="3" @if($carnet->tipo_acceso==3) selected="" @endif>Rojo</option>
                             <option value="4" @if($carnet->tipo_acceso==4) selected="" @endif>Todos</option>
                        </select>
                      </div>                    

                    </div>

                  </div>
                </div>  
              <?php 
              $archivo2 = 'img/personal/'.$carnet->url_imagen;
              if (Storage::disk('public')->exists($archivo2) == true)
              {
                $archivo = Storage::disk('public')->url($archivo2);
              }
              else{
                $archivo = "sneat/assets/img/elements/5.jpg";
              }

               $archivo2 = 'img/tipo/'.$carnet->imagentipo;
              if (Storage::disk('public')->exists($archivo2) == true)
              {
                $archivot = Storage::disk('public')->url($archivo2);
              }
              else{
                $archivot = "sneat/assets/img/elements/5.jpg";
              }
             ?>   
            <div class="col-md-6">
                  <div class="card mb-4">
                    <h5 class="card-header">Foto</h5>
                    <div class="card-body">
                      <div class="form-floating">
                        <input type="file" class="form-control" id="url_imagen" name="url_imagen">
                        <label for="floatingInput">Seleccione Imagen</label>
                        <div id="floatingInputHelp" class="col-md-4">
                          <img class="card-img card-img-right" src="{{ asset($archivo) }}" id="imgSalida" alt="Card image cap">
                         
                        </div>
                      </div>
                    </div>
                  </div>
                     <div class="card mb-4">
                    <h5 class="card-header">Modelo Plantilla</h5>
                    <div class="card-body">
                      <div class="form-floating">
                        
                        <div id="floatingInputHelp" class="col-md-4">
                          <img class="card-img-top" src="{{ asset($archivot) }}" id="imgModelo" alt="Modelo">
                         
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
<div class="col-md-6">
  <div class="card mb-2">
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
</div>
                </div>
                </form>
                <!--/ Transactions -->
              </div>

@endsection
@push('scripts')

<script  type="text/javascript" charset="utf-8" >
   
  $('#url_imagen').change(function(e) {
      addImage(e); 
    });

   

    function addImage(e){
      var file = e.target.files[0],
      imageType = /image.*/;
      if (!file.type.match(imageType))
        return;

      var reader = new FileReader();
      reader.onload = fileOnload;
      reader.readAsDataURL(file);
    }

    function fileOnload(e) {
      var result=e.target.result;
      $('#imgSalida').attr("src",result);
    }

   

  
   
   
  



  
</script>



@endpush

