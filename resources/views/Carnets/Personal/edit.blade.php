@extends('layouts.layout')
@section('titulo', 'Carnetizacion')
@section('titulo2', 'Personal - Editar')
 @section('link_back',route("personal.index"))
@section('link_new_none','d-none')
@section('content')
@section('content')
            <div class="container-xxl flex-grow-1 container-p-y">
                <form action="{{ route('personal.update',$personal->id)}}" method="POST" enctype="multipart/form-data" >
                  {{ csrf_field() }}
@method('PUT') 
<input type="hidden" name="id" value="{{ $personal->id }}">
              <div class="row">
              <div class="col-md-6">
                  <div class="card mb-4">
                    <h5 class="card-header">Crear</h5>
                    <div class="card-body">
                      <div>
                        <label for="defaultFormControlInput" class="form-label">Cédula</label>
                        <input type="text" class="form-control" id="cedula" name="cedula" placeholder="Cédula" value="{{ $personal->cedula }}" aria-describedby="defaultFormControlHelp">
                       
                      </div>
                    </div>

                   <div class="card-body">
                     <div class="mb-3">
                          <label class="form-label" for="basic-default-fullname">Apellidos</label>
                          <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="John Doe" value="{{ $personal->apellidos }}">
                        </div>
                        <div class="mb-3">
                          <label class="form-label" for="basic-default-fullname">Nombres</label>
                          <input type="text" class="form-control" id="nombres" name="nombres" placeholder="John Doe" value="{{ $personal->nombres }}">
                        </div>    
                       
                       <div class="mb-3">
                        <label for="defaultSelect" class="form-label">Cargo</label>
                        <select id="id_cargo" name="id_cargo" class="form-select">
                          <option value="0">Seleccione</option>
                          @foreach($cargos as $key)
                            <option value="{{$key->id}}" @if($key->id==$personal->id_cargo) selected @endif>{{$key->descripcion}}</option>
                          @endforeach
   
                        </select>
                      </div> 
                      <div class="mb-3">
                        <label for="defaultSelect" class="form-label">Ubicación</label>
                        <select id="id_ubicacion" name="id_ubicacion" class="form-select">
                          <option value="0">Seleccione</option>
                          @foreach($ubicacion as $key)
                            <option value="{{$key->id}}" @if($key->id==$personal->id_ubicacion) selected @endif>{{$key->descripcion}}</option>
                          @endforeach   
                        </select>
                      </div> 
                      <div class="mb-3">
                          <label class="form-label" for="basic-default-fullname">Fecha de Ingreso</label>
                          <input type="date" class="form-control" id="fecha_ingreso" name="fecha_ingreso" placeholder="John Doe" value="{{ $personal->fecha_ingreso }}">
                        </div> 


                    </div>

                  </div>
                </div> 
 <?php

 $archivo = 'img/personal/'.$personal->url_imagen;
 if (Storage::disk('public')->exists($archivo) == true)
    {
      $archivo = Storage::disk('public')->url($archivo);
    }
  else{
    $archivo="sneat/assets/img/elements/5.jpg";
   }
   
?>

            <div class="col-md-6">
                  <div class="card mb-4">
                    <h5 class="card-header">Foto</h5>
                    <div class="card-body">
                      <div class="form-floating">
                        <input type="file" class="form-control" id="url_imagen" name="url_imagen">
                        <label for="floatingInput">Seleccione Imagen</label>
                        <div id="floatingInputHelp" class="col-md-4">
                          <img class="card-img card-img-right" src="{{ asset($archivo) }}" id="imgSalida" alt="Card image cap">
                         
                        </div>
                      </div>
                    </div>
        </div>
    </div>
<div class="col-md-6">
  <div class="card mb-2">
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
</div>
                </div>
                </form>
                <!--/ Transactions -->
              </div>

@endsection

@push('scripts')

<script  type="text/javascript" charset="utf-8" >
   
  $('#url_imagen').change(function(e) {
      addImage(e); 
    });

   

    function addImage(e){
      var file = e.target.files[0],
      imageType = /image.*/;
      if (!file.type.match(imageType))
        return;

      var reader = new FileReader();
      reader.onload = fileOnload;
      reader.readAsDataURL(file);
    }

    function fileOnload(e) {
      var result=e.target.result;
      $('#imgSalida').attr("src",result);
    }

   

  
   
   
  



  
</script>



@endpush

