@extends('layouts.layout')
@section('titulo', 'Carnets')
@section('titulo2', 'Personal')
 @section('link_back',route("personal.index"))
@section('link_new_none','d-none')
@section('content')
@section('content')
            <div class="container-xxl flex-grow-1 container-p-y">
                <form action="{{ route('personal.store')}}" method="POST" enctype="multipart/form-data" >
                  {{ csrf_field() }}
              <div class="row">
              <div class="col-md-6">
                  <div class="card mb-4">
                    <h5 class="card-header">Crear</h5>
                    <div class="card-body">
                      <div>
                        <label for="defaultFormControlInput" class="form-label">Cédula</label>
                        <input type="text" class="form-control" id="cedula" name="cedula" placeholder="Cédula" aria-describedby="defaultFormControlHelp">
                       
                      </div>
                    </div>

                   <div class="card-body">
                     <div class="mb-3">
                          <label class="form-label" for="basic-default-fullname">Apellidos</label>
                          <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="John Doe">
                        </div>
                        <div class="mb-3">
                          <label class="form-label" for="basic-default-fullname">Nombres</label>
                          <input type="text" class="form-control" id="nombres" name="nombres" placeholder="John Doe">
                        </div>    
                       
                       <div class="mb-3">
                        <label for="defaultSelect" class="form-label">Cargo</label>
                        <input type="text" name="cargo" id="cargo" class="form-control">
                      </div> 
                      <div class="mb-3">
                        <label for="defaultSelect" class="form-label">Ubicación</label>
                        <input type="text" name="ubicacion" id="ubicacion" class="form-control">
                      </div> 
                      <div class="mb-3">
                        <label for="defaultSelect" class="form-label">Nómina</label>
                        <input type="text" name="nomina" id="nomina" class="form-control">
                      </div> 
                      <div class="mb-3">
                          <label class="form-label" for="basic-default-fullname">Fecha de Ingreso</label>
                          <input type="date" class="form-control" id="fecha_ingreso" name="fecha_ingreso" placeholder="John Doe">
                        </div> 


                    </div>

                  </div>
                </div>  

<div class="col-md-6">
  <div class="card mb-2">
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
</div>
                </div>
                </form>
                <!--/ Transactions -->
              </div>

@endsection
@push('scripts')

<script type="text/javascript" language="javascript">


  $ = jQuery;
  jQuery(document).ready(function () {

   
  $("input#cedula").bind('keydown', function (event) {

      if(event.shiftKey)
      {
        event.preventDefault();
      }
      if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 241 )    {
      }
      else {
        if (event.keyCode < 95) {
          if (event.keyCode < 48 || event.keyCode > 57) {
            event.preventDefault();
          }
        } 
        else {
          if (event.keyCode < 96 || event.keyCode > 105) {
            event.preventDefault();
          }
        }
      }        
      ;
    });
   

   

    $("input#cedula").bind('change', function (event) {

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

      });
      $.ajax({
        type: "GET",
        url: '{{ url('buscar_ciudadano') }}',
        dataType: "json",
        data: { cedula: $(this).val() , _token: '{{csrf_token()}}' },
        success: function (data){
              //var dataJson = eval(data);
              if(data.status == 'ok'){
                console.log(data);
                $('#apellidos').val(data.saime.primerapellido+" "+data.saime.segundoapellido);
                $('#nombres').val(data.saime.primernombre+" "+data.saime.segundonombre);
          
            //$('#url_imagen').val(data.img);
                //alert(data.result.primernombre);
              }
              if(data.status == 'No'){
                  alert('Trabajador No Registrado');
              }
              
              
              }
            });


    });
      
   

$('#url_imagen').change(function(e) {
      addImage(e); 
    });

   

    function addImage(e){
      var file = e.target.files[0],
      imageType = /image.*/;
      if (!file.type.match(imageType))
        return;

      var reader = new FileReader();
      reader.onload = fileOnload;
      reader.readAsDataURL(file);
    }

    function fileOnload(e) {
      var result=e.target.result;
      $('#imgSalida').attr("src",result);
    }

   
    
   

    



  });



</script>

@endpush 

