<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trabajador extends Model
{
    
    protected $connection = 'dcomun';
    protected $table = 'jos_intranet_datos_trabajador';

    protected $fillable = ['id', 'cedula', 'nombres', 'apellidos', 'sexo', 'fecha_ingreso', 'fecha_nacimiento', 'sueldo_basico', 'direccion_habitacion', 'cargo', 'c_encargado', 'ubicacion_admin', 'situacion', 'rif', 'categoria'];
    
    public function scopeCedula($query,$cedula)
    {
      //dd("scope: ".$cedula);
       $query->where('cedula','=',$cedula)->where('situacion','!=',1)->where('situacion','!=',15)->where('situacion','!=',4)->where('situacion','!=',7);
    }
}
