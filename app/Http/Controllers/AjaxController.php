<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Personal;
use App\Cargos;
use App\Cargosn;
use App\Ciudadanos;
use App\Ubicacion;
use App\Ubicacionn;
use App\Tipos;
use App\Recibo;
use App\Trabajador;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Auth;

class AjaxController extends Controller
{
  
    public function buscar(Request $request)
    {
      $cadena = $request["valor"]."%";  
   
      $personal = Personal::leftjoin('cargos as c','personal.id_cargo','=','c.id')->leftjoin('ubicacion as u','personal.id_ubicacion','=','u.id')->select('personal.cedula','personal.apellidos','personal.nombres','personal.fecha_ingreso','c.descripcion as cargo','personal.id_cargo','personal.url_imagen','u.descripcion as ubicacion','personal.id_ubicacion')->where('cedula', 'LIKE', $cadena)->orderBy('cedula', 'asc')->first();
      #dd($personal);
      $archivo = 'img/personal/'.$personal->url_imagen;
 if (Storage::disk('public')->exists($archivo) == true)
    {
      $archivo = Storage::disk('public')->url($archivo);
    }
  else{
    $archivo="sneat/assets/img/elements/5.jpg";
   }      
      $data["personal"]=$personal;
      $data["img"]=$archivo;   
      return $data;
    }

   public function buscarModelo(Request $request)
    {
      $cadena = $request["valor"];  
   
      $tipo = Tipos::where('id',$cadena)->first();
      #dd($personal);
      $archivo = 'img/tipo/'.$tipo->url_imagen;
 if (Storage::disk('public')->exists($archivo) == true)
    {
      $archivo = Storage::disk('public')->url($archivo);
    }
  else{
    $archivo="sneat/assets/img/elements/5.jpg";
   }      
      
      $data["img"]=$archivo;   
      return $data;
    }




    public function buscar_trabajador(Request $request)
    {
      $request->all();
      $edad=0;
      $cedula =$request["cedula"];
      if(isset($request["cedula"])){
        $recibo = Recibo::Ultimo($cedula)->first();
                if($recibo!=NULL) $tipo = $recibo->tipo;
          $trabajador= trabajador::cedula($request->get("cedula"))->where('tipo',$tipo)->first();
          
          if(!isset($trabajador->cedula)){
               $data["status"]='No';
                $data["result"]='';
                $data["edad"]=$edad;
          }      
          else{  
          
          $fecha_nacimiento = $trabajador->fecha_nacimiento;
         
          $codcargo = $trabajador->cargo;
          $tipo = $trabajador->tipo;
          switch ($tipo) {
            case '1':  
              $tipon = "ALTO NIVEL";
              break;
            case '2':
               $tipon = "EMPLEADOS";
               break;
            case '3':
               $tipon = "JUBILADOS";
               break;
            case '4':
               $tipon = "OBREROS";
               break;

          }
          $categoria = $trabajador->categoria;
        
          if($categoria=="27" || $categoria=="28") $tipon="CONTRATADOS";

         # $cargos = cargos::where('cod_concepto',$codcargo)->where('tipo',$tipo)->first();
          $cargos = Cargosn::where('tab_cdg',$codcargo)->where('tipo',$tipo)->where('tab_nrotab',3)->first();
    
          if($cargos) $cargo = $cargos->tab_dscrip;
          else $cargo="";

          $codubicacion = $trabajador->ubicacion_admin;
          $ubicacions = Ubicacionn::where('tab_cdg',$codubicacion)->where('tipo',$tipo)->where('tab_nrotab',1)->first();
          if($ubicacions!=null) $ubicacion = $ubicacions->tab_dscrip;
          else
          $ubicacion="";           
           
           $data["result"]    = $trabajador;
          
           $data["cargo"]     = $cargo;
           $data["ubicacion"]     = $ubicacion;
           $data["nomina"]    = $tipon;
           $data["ingreso"]    = $trabajador->fecha_ingreso;
            $data["status"]='Ok';
           $tipo = Tipos::where('descripcion',$tipon)->first();
      #dd($personal);
           $archivo = 'img/tipo/'.$tipo->url_imagen;
           if (Storage::disk('public')->exists($archivo) == true)
           {
               $archivo = Storage::disk('public')->url($archivo);
           }
            else{
              $archivo="sneat/assets/img/elements/5.jpg";
            }      
            $data["img"]=$archivo;   

          }
        }         
          return $data;

    }


    public function buscar_ciudadano(Request $request)
    {
        $ced = $request["cedula"];
        $nac = "V";
         $saime = Ciudadanos::where('nac',$nac)->where('cedula',$ced)->first();
         if($saime->cedula!=NULL){
          $data["status"]="ok";
          $data["saime"]=$saime;
         }else{
          $data["status"]="no";
         }
         return $data;
    }

}



