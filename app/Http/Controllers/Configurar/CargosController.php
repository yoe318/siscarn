<?php

namespace App\Http\Controllers\Configurar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Cargos;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Auth;

class CargosController extends Controller
{
     public function __construct()
    {
      $this->middleware('auth');
    }

    public function index()
    {
       $cargos = Cargos::orderby('id','asc')->get();
       return view('Configurar.Cargos.index',compact('cargos'));
    }

    public function create()
    {
      return view('Configurar.cargos.create');
    }

    public function edit($id)
    {
        $cargo = Cargos::where('id',$id)->first();
        return view('Configurar.Cargos.edit',compact('cargo'));
    }
    public function show($id)
    {
        
    }

    public function store(Request $request)
    {
      $descripcion = $request["descripcion"];
      if($descripcion!=""){
        $cargos = Cargos::where('descripcion',$descripcion)->first();
        if($cargos!=null){
           \Session::flash('mensajeError', 'Cargo Ya Registrado');
        }else{


             $cargo = new Cargos;
             $cargo->descripcion = $descripcion; 

             $cargo->id_usuario = auth()->id();  
             $cargo->save();  
             \Session::flash('mensaje', 'Cargo creado con éxito');
        }
      }
      return redirect()->route("cargos.index");
    }

    public function update(Request $request,$id)
    {
        $cargo = cargos::where('id',$id)->first();
        if($cargo!=null){
                        
        
             $cargo->descripcion = $request->descripcion; 
             if($name!="")$cargo->url_imagen = $name;
             #$cargo->id_estatus = 1;
             $cargo->save();  
             \Session::flash('mensaje', 'cargo actualizado con éxito'); 
             }
              return redirect()->route("cargos.index");   
    }

}
