<?php

namespace App\Http\Controllers\Configurar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Ubicacion as Ubicaciones;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Auth;

class UbicacionController extends Controller
{
     public function __construct()
    {
      $this->middleware('auth');
    }

    public function index()
    {
       $ubicaciones = Ubicaciones::orderby('id','asc')->get();
       return view('Configurar.Ubicaciones.index',compact('ubicaciones'));
    }

    public function create()
    {
      return view('Configurar.Ubicaciones.create');
    }

    public function edit($id)
    {
        $cargo = Ubicaciones::where('id',$id)->first();
        return view('Configurar.Ubicaciones.edit',compact('cargo'));
    }
    public function show($id)
    {
        
    }

    public function store(Request $request)
    {
      $descripcion = $request["descripcion"];
      if($descripcion!=""){
        $ubicaciones = Ubicaciones::where('descripcion',$descripcion)->first();
        if($ubicaciones!=null){
           \Session::flash('mensajeError', 'Ubicación Ya Registrado');
        }else{


             $cargo = new Ubicaciones;
             $cargo->descripcion = $descripcion; 

             $cargo->id_usuario = auth()->id();  
             $cargo->save();  
             \Session::flash('mensaje', 'Ubicación creado con éxito');
        }
      }
      return redirect()->route("ubicaciones.index");
    }

    public function update(Request $request,$id)
    {
        $cargo = Ubicaciones::where('id',$id)->first();
        if($cargo!=null){
                        
        
             $cargo->descripcion = $request->descripcion; 
             $cargo->save();  
             \Session::flash('mensaje', 'Ubicación actualizado con éxito'); 
             }
              return redirect()->route("ubicaciones.index");   
    }

}
