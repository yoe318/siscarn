<?php

namespace App\Http\Controllers\Configurar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Tipos;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Auth;

class TiposController extends Controller
{
     public function __construct()
    {
      $this->middleware('auth');
    }

    public function index()
    {
       $tipos = Tipos::orderby('id','asc')->get();
       return view('Configurar.Tipos.index',compact('tipos'));
    }

    public function create()
    {
      return view('Configurar.Tipos.create');
    }

    public function edit($id)
    {
        $tipo = Tipos::where('id',$id)->first();
        return view('Configurar.Tipos.edit',compact('tipo'));
    }
    public function show($id)
    {
        
    }

    public function store(Request $request)
    {
      $descripcion = $request["descripcion"];
      if($descripcion!=""){
        $tipos = Tipos::where('descripcion',$descripcion)->first();
        if($tipos!=null){
           \Session::flash('mensajeError', 'Tipo Ya Registrado');
        }else{

        if($request->hasFile('url_imagen')){
                   $nombre ="tipo ";
                   $archivo_nombre =Str::slug($nombre, '-');
                   $archivo_nombre = $archivo_nombre.'-'.Carbon::now()->format('YmdHis');       
                     $name = $archivo_nombre.'.'.$request->url_imagen->getClientOriginalExtension();
                 $name_file = Storage::disk('public')->putFileAs('img/tipo', new File($request->url_imagen), $name,'public');
              
                 }else{$name="";} 

             $tipo = new Tipos;
             $tipo->descripcion = $descripcion; 
             $tipo->url_imagen = $name;
             $tipo->id_estatus = 1;
             $tipo->id_usuario = auth()->id();  
             $tipo->save();  
             \Session::flash('mensaje', 'Tipo creado con éxito');
        }
      }
      return redirect()->route("tipos.index");
    }

    public function update(Request $request,$id)
    {
        $tipo = Tipos::where('id',$id)->first();
        if($tipo!=null){
          if($request->hasFile('url_imagen')){
                       $nombre ="tipo ";
                       $archivo_nombre =Str::slug($nombre, '-');
                       $archivo_nombre = $archivo_nombre.'-'.Carbon::now()->format('YmdHis');       
                         $name = $archivo_nombre.'.'.$request->url_imagen->getClientOriginalExtension();
                     $name_file = Storage::disk('public')->putFileAs('img/tipo', new File($request->url_imagen), $name,'public');
                  
                     }else{$name="";} 
                            
        
             $tipo->descripcion = $request->descripcion; 
             if($name!="")$tipo->url_imagen = $name;
             #$tipo->id_estatus = 1;
             $tipo->save();  
             \Session::flash('mensaje', 'Tipo actualizado con éxito'); 
             }
              return redirect()->route("tipos.index");   
    }

}
