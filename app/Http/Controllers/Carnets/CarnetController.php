<?php

namespace App\Http\Controllers\Carnets;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Personal;
use App\Carnets;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Carbon\Carbon;
use App\Cargos;
use App\Tipos as Tipo;
use Illuminate\Support\Str;
use App\libreria;
use Barryvdh\DomPDF\Facade as PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Facades\Cache;

class CarnetController extends Controller
{
     public function __construct()
    {
      $this->middleware('auth');
    }
   public function index(Request $request)
   {
     $carnet= Carnets::orderby('carnets.id','desc')->get();
     return view('Carnets.Procesar.index',compact('carnet')); 
   }

   public function create()
   {
    $tipo = Tipo::where('id_estatus',1)->get();
    return view('Carnets.Procesar.crear',compact('tipo'));


   }

   public function edit($id)
   {
    $tipo = Tipo::where('id_estatus',1)->get();
     $carnet= Carnets::join('personal as b','b.id','=','carnets.id_personal')->leftjoin('cargos as c','c.id','=','b.id_cargo')->leftjoin('ubicacion as u','u.id','=','b.id_cargo')->leftjoin('tipo as t','t.id','=','carnets.id_tipo')->select('b.cedula','b.apellidos','b.nombres','carnets.id_tipo','carnets.id','carnets.created_at as creado','c.descripcion as cargo','b.fecha_ingreso','b.url_imagen','t.url_imagen as imagentipo','carnets.tipo_acceso','carnets.codigo_seguridad','u.descripcion as ubicacion')->orderby('carnets.id','desc')->where('carnets.id',$id)->first();
     return view('Carnets.Procesar.edit',compact('carnet','tipo'));
   }

   public function store(Request $request)
   {
     //dd($request);
     //$personal = Personal::where('cedula',$request->cedula)->first();
      if($request->hasFile('url_imagen')){
                   $nombre ="personal";
                   $archivo_nombre =Str::slug($nombre, '-');
                   $archivo_nombre = $archivo_nombre.'-'.Carbon::now()->format('YmdHis');       
                     $name = $archivo_nombre.'.'.$request->url_imagen->getClientOriginalExtension();
                 $name_file = Storage::disk('public')->putFileAs('img/personal', new File($request->url_imagen), $name,'public');
              $name = $name_file;
      }else{$name="";} 
     

     $carnets= new Carnets;
     $carnets->cedula = $request->cedula;
     $carnets->codigo_seguridad = $request->codigo_seguridad;
     $carnets->tipo_acceso = $request->tipo_acceso;
     $carnets->tipo = $request->tipo;
     $carnets->url_imagen = $name;
     $carnets->id_estatus = 1;
     $carnets->id_usuario   = auth()->id();  
     $carnets->save();
     $idcarnet = $carnets->id;
     // $carnet = Carnets::where('id_personal',$personal->id)->where('id','!=',$idcarnet)->get();
     // foreach($carnet as $car)
     // {
     //   $car->id_estatus =2;
     //   $car->save();
     // }

     \Session::flash('mensaje', 'Carnet creado con éxito');
        
      return redirect()->route("carnet.index");
   }

   public function imprimir($id)
   {
    $carnet= Carnets::join('personal as b','b.id','=','carnets.id_personal')->join('cargos as c','c.id','=','b.id_cargo')->leftjoin('tipo as t','t.id','=','carnets.id_tipo')->leftjoin('ubicacion as u','u.id','=','b.id_cargo')->select('b.cedula','b.apellidos','b.nombres','carnets.id_tipo','carnets.id','carnets.created_at as creado','c.descripcion as cargo','b.fecha_ingreso','b.url_imagen','t.url_imagen as imagentipo','carnets.tipo_acceso','carnets.codigo_seguridad','u.descripcion as ubicacion')->orderby('carnets.id','desc')->where('carnets.id',$id)->first();
     $valor = substr($this->crea_token($id),-4);
     $carnets = Carnets::where('id',$id)->first();
     $carnets->token = $valor;
     $carnets->save();
     $qrname = '../public/qrcodes/'.$id.'.svg';
     $url = config('app.url')."/app/carnets/".$carnets->token;
     $valor = QrCode::encoding('UTF-8')->size(600)->format('svg')->generate($url,$qrname);
     #dd($qrname);
       #$dompdf->set_paper(array(0, 0, 595, 841), 'portrait');
      #$pdf = PDF::loadView('Carnets.Procesar.carnetpdf', compact('carnet', 'qrname','url'))->setPaper('a4', 'landscape');
       $pdf = PDF::loadView('Carnets.Procesar.carnetpdf', compact('carnet', 'qrname','url'))->setPaper(array(0, 0, 420, 580), 'portrait');

       $name = "carnet_".$carnets->token;
        // return $pdf->download('directiva.pdf');
        return $pdf->stream($name);    
   }

   public function update(Request $request,$id)
   {
      if($request->hasFile('url_imagen')){
                   $nombre ="personal";
                   $archivo_nombre =Str::slug($nombre, '-');
                   $archivo_nombre = $archivo_nombre.'-'.Carbon::now()->format('YmdHis');       
                     $name = $archivo_nombre.'.'.$request->url_imagen->getClientOriginalExtension();
                 $name_file = Storage::disk('public')->putFileAs('img/personal', new File($request->url_imagen), $name,'public');
              
      }else{$name="";} 
      if($name!=""){
        if($personal->url_imagen!=""){
           $filename_old = "img/personal/".$personal->url_imagen;
               Storage::disk('public')->delete($filename_old);
          $personal->url_imagen = $name;
        }
        $personal->url_imagen = $name;
        $personal->save();
      }
     $carnets= Carnets::where('id',$id)->first();
     $carnets->codigo_seguridad      = $request->codigo_seguridad;
     $carnets->tipo_acceso = $request->tipo_acceso;
     $carnets->id_tipo = $request->id_tipo;
     $carnets->id_estatus = 1;
     $carnets->save();
     \Session::flash('mensaje', 'Carnet actualizado con éxito');
        
      return redirect()->route("carnet.index");
   }

   public function destroy($id)
   {

   }

   public function anular($id)
   {

   }


function crea_token($valor){
  $posicion=rand(1,3);
  $respuesta=$this->codifica(100000 + $posicion);
  for($l=1; $l<=3; $l++){
    if($l==$posicion)
      $respuesta.= "$" . $this->codifica(100000 + $valor);
    else
      $respuesta.= "$" . $this->codifica(rand(100000,999999));
  }
  return $respuesta;
}

function decodifica_token($valor){
  $mvalor=explode("$",$valor);
  if (count($mvalor)<4) return '';
  $posicion=$this->decodifica($mvalor[0]);
  if($posicion<>''){
    $posicion-=100000;
  }else{
    return '';
  }
  return decodifica($mvalor[$posicion]) - 100000;
}


function codifica($valor){
  $valor+=1000;
  $mrcd=array( 0 =>'A', 1 =>'B', 2 =>'C', 3 =>'D', 4 =>'E', 5 =>'F', 6 =>'G', 7 =>'H', 8 =>'I', 9 =>'J',10 =>'K',11 =>'L',12 =>'M',13 =>'N',14 =>'O',15 =>'P',16 =>'Q',17 =>'R',18 =>'S',19 =>'T',20 =>'U',21 =>'V',22 =>'W',23 =>'X',24 =>'Y',25 =>'Z',26 =>'1',27 =>'2',28 =>'3',29 =>'4',30 =>'5',31 =>'6',32 =>'7',33 =>'8',34 =>'9',35 =>'0',36 =>'a',37 =>'b',38 =>'c',39 =>'d',40 =>'e',41 =>'f',42 =>'g',43 =>'h',44 =>'i',45 =>'j',46 =>'k',47 =>'l',48 =>'m',49 =>'n',50 =>'o',51 =>'p',52 =>'q',53 =>'r',54 =>'s',55 =>'t',56 =>'u',57 =>'v',58 =>'w',59 =>'x',60 =>'y',61 =>'z');
  $separador=rand(0,6);
  //$separador=0;
  $salida="";
    while($valor>55){
    @$division    = $valor/55;
    @$resultINT   = floor($valor/55);
    @$remnant     = $valor%55;
    $salida  = $mrcd[$remnant+$separador].
    $mrcd[61-($remnant+$separador)].$salida;
    $valor=$resultINT;
  }
  $salida  = $mrcd[$separador*5] . $mrcd[$valor+$separador]. $mrcd[61-($valor+$separador)].$salida;
  return $salida;
}

function decodifica($valor){
  $mrcd=array( 0 =>'A', 1 =>'B', 2 =>'C', 3 =>'D', 4 =>'E', 5 =>'F', 6 =>'G', 7 =>'H', 8 =>'I', 9 =>'J',10 =>'K',11 =>'L',12 =>'M',13 =>'N',14 =>'O',15 =>'P',16 =>'Q',17 =>'R',18 =>'S',19 =>'T',20 =>'U',21 =>'V',22 =>'W',23 =>'X',24 =>'Y',25 =>'Z',26 =>'1',27 =>'2',28 =>'3',29 =>'4',30 =>'5',31 =>'6',32 =>'7',33 =>'8',34 =>'9',35 =>'0',36 =>'a',37 =>'b',38 =>'c',39 =>'d',40 =>'e',41 =>'f',42 =>'g',43 =>'h',44 =>'i',45 =>'j',46 =>'k',47 =>'l',48 =>'m',49 =>'n',50 =>'o',51 =>'p',52 =>'q',53 =>'r',54 =>'s',55 =>'t',56 =>'u',57 =>'v',58 =>'w',59 =>'x',60 =>'y',61 =>'z');
  $separador=0;
  $mvalor=str_split($valor);
  $resultado1=0;
  $resultado2=0;
  for($i=0;$i<=61;$i++)
    if($mvalor[0]==$mrcd[$i])
      $separador=$i/5;
      
  $contexp=((count($mvalor)-1)/2)-1;
  for($l=1;$l<count($mvalor);$l+=2){
    for($i=0;$i<=61;$i++){
      if($mvalor[$l]==$mrcd[$i]){
        $resultado1+=($i-$separador)*pow(55,$contexp);
      }
      if($mvalor[$l+1]==$mrcd[$i]){
        $resultado2+=(61-($i+$separador))*pow(55,$contexp);
      }
    }
    $contexp--;
  }
  if($resultado1==$resultado2 and is_int($separador)){
    return $resultado1 - 1000;
  }else{
    return "";
  }
}



}
