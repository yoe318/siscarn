<?php

namespace App\Http\Controllers\Carnets;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Personal;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Carbon\Carbon;
use App\Cargos;
use App\Ubicacion;
use App\Tipos;
use App\Carnets;
use Illuminate\Support\Str;
use Auth;

class PersonalController extends Controller
{
     public function __construct()
    {
      $this->middleware('auth');
    }

    public function index()
    {
       $personal = Personal::orderby('id','asc')->get();
       return view('Carnets.Personal.index',compact('personal'));
    }

    public function create()
    {
      
      return view('Carnets.Personal.create');
    }

    public function edit($id)
    {
        $personal = Personal::where('id',$id)->first();
      $cargos = Cargos::orderby('descripcion','asc')->get();
      $ubicacion = Ubicacion::orderby('descripcion','asc')->get();
        return view('Carnets.Personal.edit',compact('personal','cargos','ubicacion'));
    }
    public function show($id)
    {
        
    }

    public function store(Request $request)
    {

      $cedula = $request["cedula"];
      if($cedula!=""){
        $personal = Personal::where('cedula',$cedula)->first();
        if($personal!=null){
           \Session::flash('mensajeError', 'Cédula Ya Registrada');
        }else{

        if($request->hasFile('url_imagen')){
                   $nombre ="personal";
                   $archivo_nombre =Str::slug($nombre, '-');
                   $archivo_nombre = $archivo_nombre.'-'.Carbon::now()->format('YmdHis');       
                     $name = $archivo_nombre.'.'.$request->url_imagen->getClientOriginalExtension();
                 $name_file = Storage::disk('public')->putFileAs('img/personal', new File($request->url_imagen), $name,'public');
                 
                 }else{$name="";} 

             $personal = new Personal;
             $personal->cedula       = $cedula; 
             $personal->apellidos    = $request->apellidos;
             $personal->nombres      = $request->nombres;
             $personal->cargo        = $request->cargo;
             $personal->ubicacion    = $request->ubicacion; 
             $personal->id_estatus   = 1;
             $personal->fecha_ingreso = $request->fecha_ingreso;
             $personal->id_usuario   = auth()->id();  
             $personal->save();  
             

             \Session::flash('mensaje', 'Personal creado con éxito');
        }
      }
      return redirect()->route("personal.index");
    }

    public function update(Request $request,$id)
    {
        $personal = Personal::where('id',$id)->first();
        if($personal!=null){
          if($request->hasFile('url_imagen')){
                $nombre ="personal ";
                $archivo_nombre =Str::slug($nombre, '-');
                $archivo_nombre = $archivo_nombre.'-'.Carbon::now()->format('YmdHis');       
                $name = $archivo_nombre.'.'.$request->url_imagen->getClientOriginalExtension();
                $name_file = Storage::disk('public')->putFileAs('img/personal', new File($request->url_imagen), $name,'public');
               
            }else{$name="";} 
                                
        
             if($name!="")$personal->url_imagen = $name;
             #$personal->id_estatus = 1;
             $personal->apellidos    = $request->apellidos;
             $personal->nombres      = $request->nombres;
             $personal->id_cargo     = $request->id_cargo;
             $personal->id_ubicacion = $request->id_ubicacion; 
             $personal->url_imagen   = $name;
             $personal->id_estatus   = 1;
             $personal->save();  
             \Session::flash('mensaje', 'personal actualizado con éxito'); 
             }
              return redirect()->route("personal.index");   
    }

}
