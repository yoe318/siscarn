<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ubicacion extends Model
{
     protected $connection = 'dcomun';
   protected $table = 'ubicacionesv';
/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','cod_concepto','concepto','nombre_corto','estatus'];
    protected $guarded  = ['id'];
}
