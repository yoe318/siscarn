<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class Recibo extends Model
{
   
       protected $connection = 'dcomun';
    protected $table = 'jos_intranet_datos_nomina_cal';

    public function scopeUltimo($query,$cedula)
    {
       $query ->select(db::raw('MAX(cal_fchact) as fecha'),'tipo')->where('cedula',$cedula)->orderby('cal_fchact','desc')->orderby('tipo','desc')->groupby('tipo','cal_fchact');
    }
}
