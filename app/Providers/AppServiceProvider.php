<?php

namespace App\Providers;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Auth;

use App\Roles;
use DB;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      Schema::defaultStringLength(191);
      Carbon::setLocale('es');
      Carbon::setUTF8(true);
      setlocale(LC_TIME, 'es_ES');

      view()->composer('*', function($view) {
        if(Auth::user())
             {    
               $name_user= Auth::user()->name;
               $id_usuario =Auth::user()->id;
               $rol = DB::table('role_user as a')->join('roles as b','b.id','=','a.role_id')->select('a.role_id','a.user_id','b.descripcion')->where('user_id',$id_usuario)->first();
                 if($rol){
                   $perfil = $rol->descripcion; 
                   $idperfil = $rol->role_id; 
                 } 
                 else{
                   $perfil="";
                   $idperfil=0;  
                }
              }else{
                   $perfil="";
                   $idperfil=0;
                   $name_user="";
                   $id_usuario = 0;
              }  
   
            $view->with('idperfil', $idperfil)->with('perfil',$perfil)->with('name',$name_user);
  
          });

    }


    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
